﻿using System;
using System.Linq;
using Castle.Core.Internal;
using Castle.DynamicProxy;
using Oxygen.AOP.Aspects;

namespace Oxygen.AOP.Interceptors
{

    public class Interceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            var aspects = invocation.GetConcreteMethodInvocationTarget()
                .GetCustomAttributes(typeof(AspectAttribute), true).Cast<AspectAttribute>();

            if (!aspects.Any())
            {
                invocation.Proceed();
                return;
            }
            aspects.ForEach(CallOnMethodExecutingFor(invocation));
            try
            {
                invocation.Proceed();
                aspects.ForEach(CallOnMethodExecutedFor(invocation));
            }
            catch (Exception ex)
            {
                var methodContext = GetMethodErroredContext(invocation, ex);
                aspects.ForEach(aspect => aspect.OnMethodErrored(methodContext));
                if (!aspects.Any(a => a.EatAllExceptions))
                    throw;

                invocation.ReturnValue = methodContext.ReturnValue;
            }
            finally
            {
                SetDefaultValueForValueTypesIfExceptionOccuredIn(invocation);
            }
        }

        private static MethodErroredContext GetMethodErroredContext(IInvocation invocation, Exception ex)
        {
            var methodContext = new MethodErroredContext(
                invocation.Arguments,
                invocation.GenericArguments,
                invocation.GetConcreteMethodInvocationTarget(),
                invocation.InvocationTarget,
                ex);
            return methodContext;
        }
        
        private Action<AspectAttribute> CallOnMethodExecutedFor(IInvocation invocation)
        {
            return aspect =>
            {
                var methodContext = new MethodExecutedContext(
                    invocation.Arguments,
                    invocation.GenericArguments,
                    invocation.GetConcreteMethodInvocationTarget(),
                    invocation.InvocationTarget,
                    invocation.ReturnValue
                    );
                aspect.OnMethodExecuted(methodContext);
            };
        }

        private Action<AspectAttribute> CallOnMethodExecutingFor(IInvocation invocation)
        {
            return aspect =>
            {
                var methodContext = new MethodContext(
                    invocation.Arguments,
                    invocation.GenericArguments,
                    invocation.GetConcreteMethodInvocationTarget(), invocation.InvocationTarget);
                aspect.OnMethodExecuting(methodContext);
            };
        }

        private void SetDefaultValueForValueTypesIfExceptionOccuredIn(IInvocation invocation)
        {
            if (!invocation.Method.ReturnType.IsClass
                && !invocation.Method.ReturnType.IsInterface
                && invocation.Method.ReturnType.Name != "Void"
                && invocation.Method.ReturnParameter != null
                && invocation.ReturnValue == null)
                invocation.ReturnValue = Activator.CreateInstance(invocation.Method.ReturnType);
        }
    }
}