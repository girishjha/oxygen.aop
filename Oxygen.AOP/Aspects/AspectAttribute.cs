﻿using System;

namespace Oxygen.AOP.Aspects
{
    public abstract class AspectAttribute : Attribute
    {

        public virtual void OnMethodExecuting(MethodContext methodContext)
        {
            
        }
        public virtual void OnMethodExecuted(MethodExecutedContext methodContext)
        {
            
        }
        public bool EatAllExceptions { get; set; }

        public virtual void OnMethodErrored(MethodErroredContext methodContext)
        {
            
        }
    }
}