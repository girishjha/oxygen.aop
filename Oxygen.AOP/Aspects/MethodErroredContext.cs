﻿using System;
using System.Reflection;

namespace Oxygen.AOP.Aspects
{
    public class MethodErroredContext
    {
        public MethodErroredContext(object[] arguments, Type[] genericArguments, MethodInfo getConcreteMethodInvocationTarget, object invocationTarget, Exception exception)
        {
            Arguments = arguments;
            GenericArguments = genericArguments;
            GetConcreteMethodInvocationTarget = getConcreteMethodInvocationTarget;
            InvocationTarget = invocationTarget;
            Error = exception;
        }
        public object[] Arguments { get; private set; }
        public Type[] GenericArguments { get; private set; }
        public MethodInfo GetConcreteMethodInvocationTarget { get; private set; }
        public object InvocationTarget { get; private set; }
        public Exception Error { get; private set; }
        public object ReturnValue { get; set; }
    }
}