using System;
using System.Reflection;

namespace Oxygen.AOP.Aspects
{
    public class MethodContext
    {
        public object[] Arguments { get; private set; }
        public Type[] GenericArguments { get;private set; }
        public MethodInfo MethodInvocationTarget { get; private set; }
        public object InvocationTarget { get; private set; }

        public MethodContext(object[] arguments, Type[] genericArguments, MethodInfo methodInvocationTarget, object invocationTarget)
        {
            Arguments = arguments;
            GenericArguments = genericArguments;
            MethodInvocationTarget = methodInvocationTarget;
            InvocationTarget = invocationTarget;
        }
    }
}