using System;
using System.Reflection;

namespace Oxygen.AOP.Aspects
{
    public class MethodExecutedContext
    {
        public object[] Arguments { get; set; }
        public Type[] GenericArguments { get; set; }
        public MethodInfo GetConcreteMethodInvocationTarget { get; set; }
        public object InvocationTarget { get; set; }
        public object ReturnValue { get; set; }

        public MethodExecutedContext(object[] arguments, Type[] genericArguments, MethodInfo getConcreteMethodInvocationTarget, object invocationTarget, object returnValue)
        {
            Arguments = arguments;
            GenericArguments = genericArguments;
            GetConcreteMethodInvocationTarget = getConcreteMethodInvocationTarget;
            InvocationTarget = invocationTarget;
            ReturnValue = returnValue;
        }
    }
}