﻿using Castle.DynamicProxy;
using Oxygen.AOP.Interceptors;
using Oxygen.AOP.Tests.App.Classes;
using StructureMap;

namespace Oxygen.AOP.Tests
{
    //Commit 12
    public class Bootsrapper
    {
        static readonly ProxyGenerator ProxyGenerator = new ProxyGenerator();

        static public void BootstrapStructureMap()
        {
            ObjectFactory.Configure(cfg =>
            {
                cfg.For<IFooo>().AlwaysUnique().Use<MyClass>().Named("MyClass");
                cfg.For<IFooo>().AlwaysUnique().Use<Foo>().Named("Foo");
                cfg.For<IFooo>().EnrichAllWith(instance => ProxyGenerator.CreateInterfaceProxyWithTarget(instance, ProxyGenerationOptions.Default, new Interceptor()));
            });
        }
    }

    interface IMyFactory
    {
        T GetInatance<T>(string name);
    }

    class MyFactory : IMyFactory
    {

        public T GetInatance<T>(string name)
        {
            return ObjectFactory.GetNamedInstance<T>(name);
        }
    }
}