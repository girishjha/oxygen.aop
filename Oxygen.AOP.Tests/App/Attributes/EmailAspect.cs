using System;
using Oxygen.AOP.Aspects;

namespace Oxygen.AOP.Tests.App.Attributes
{
    public class EmailAspect : AspectAttribute
    {
        public override void OnMethodExecuting(MethodContext methodContext)
        { 
            Console.WriteLine("From EmailAspect before the call");
        }

        public override void OnMethodExecuted(MethodExecutedContext methodContext)
        {
            Console.WriteLine("From EmailAspect after the call");

        }

        public override void OnMethodErrored(MethodErroredContext methodContext)
        {
            Console.WriteLine("From EmailAspect Exception Scenareo");
            methodContext.ReturnValue = 100;
        }
    }
}