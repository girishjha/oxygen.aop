﻿using Oxygen.AOP.Aspects;

namespace Oxygen.AOP.Tests.App.Attributes
{
    public class SomeOtherAspect :  AspectAttribute
    {
        public override void OnMethodErrored(MethodErroredContext methodContext)
        {
            methodContext.ReturnValue = "Hello I ate the exeption";
        }
    }
}