using System;
using Oxygen.AOP.Aspects;

namespace Oxygen.AOP.Tests.App.Attributes
{
    public class LoggingAspect : AspectAttribute
    {
        public override void OnMethodExecuting(MethodContext invocation)
        {
            Console.WriteLine("From LoggingAspect before call");
        }

        public override void OnMethodExecuted(MethodExecutedContext invocation)
        {
            Console.WriteLine("From LoggingAspect After call");
        }

        public override void OnMethodErrored(MethodErroredContext methodContext)
        {
            Console.WriteLine("From LoggingAspect Exception Scenareo");
            methodContext.ReturnValue = 300;
        }
    }
}