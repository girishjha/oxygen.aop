﻿using System;
using System.Collections.Generic;
using Oxygen.AOP.Tests.App.Attributes;

namespace Oxygen.AOP.Tests.App.Classes
{
    public interface IFooo
    {
        void MyMethod();
        string MethodThatReturnsSomething(int valueType, KeyValuePair<string, string> keyValuePair);
        string MethodThatReturnsSomethingAndThowException(int valueType, KeyValuePair<string, string> keyValuePair);
        int MethodThatReturnsSomeNotNullableValue(int valueType, KeyValuePair<string, string> keyValuePair);
        int MethodThatReturnsSomeNotNullableValueAndThrowException(int valueType, KeyValuePair<string, string> keyValuePair);
        int MethodThatReturnsSomeNotNullableValueWIthMoreThanOneInterceptor(int valueType, KeyValuePair<string, string> keyValuePair);
    }
 
    class MyClass : IFooo
    {
        [LoggingAspect(EatAllExceptions = true)]
        public void MyMethod()
        {
            Console.WriteLine("From method MyMethod in MyClass class");
            ExceptionMethod();
        }
        [LoggingAspect(EatAllExceptions = true)]
        public string MethodThatReturnsSomething(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            return "400";
        }

        public string MethodThatReturnsSomethingAndThowException(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            ExceptionMethod();
            return "400";
        }

        [LoggingAspect]
        public int MethodThatReturnsSomeNotNullableValue(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            Console.WriteLine("NotNull type method called");
            return 100;
        }

        public int MethodThatReturnsSomeNotNullableValueAndThrowException(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            Console.WriteLine("NotNull type method called");
            ExceptionMethod();
            return 0;
        }


        [LoggingAspect]
        [EmailAspect(EatAllExceptions = true)]
        public int MethodThatReturnsSomeNotNullableValueWIthMoreThanOneInterceptor(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            Console.WriteLine("NotNull type method called");
            ExceptionMethod();
            return 100;
        }



        public void ExceptionMethod()
        {
            throw new InvalidOperationException("There was a null ref exception");
        }
    }
    public class Foo : IFooo
    {
        public void MyMethod()
        {
            Console.WriteLine("From method MyMethod in Foo class");
            //return 0;
        }

        public string MethodThatReturnsSomething(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            throw new NotImplementedException();
        }
        [SomeOtherAspect(EatAllExceptions = true)]
        public string MethodThatReturnsSomethingAndThowException(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            throw new Exception("Exception occured");
        }

        public int MethodThatReturnsSomeNotNullableValue(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            Console.WriteLine("NotNull type method called");
            return 100;
        }

        public int MethodThatReturnsSomeNotNullableValueAndThrowException(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            throw new NotImplementedException();
        }

        public int MethodThatReturnsSomeNotNullableValueWIthMoreThanOneInterceptor(int valueType, KeyValuePair<string, string> keyValuePair)
        {
            throw new NotImplementedException();
        }
    }
    //[LoggingAspect(Index = 1)]
    
}