﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Oxygen.AOP.Tests.App.Classes;

namespace Oxygen.AOP.Tests
{
    [TestFixture]
    public class StructureMapTests
    {
        private IMyFactory _factory;

        #region Setup & TearDown

        [SetUp]
        public void Setup()
        {
            Bootsrapper.BootstrapStructureMap();
            _factory = new MyFactory();
        }

        [TearDown]
        public void TearDown()
        {
        }


        #endregion

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MethodThatReturnsSomeNotNullableValueAndThrowException()
        {
           //Commit one

            var obj = _factory.GetInatance<IFooo>("MyClass");
            //obj.MyMethod();
            //var result = obj.MethodThatReturnsSomething(55, new KeyValuePair<string, string>("Name", "Value"));
            //Assert.That(result, Is.EqualTo(null));

            var intresult = obj.MethodThatReturnsSomeNotNullableValueAndThrowException(99, new KeyValuePair<string, string>());
            Assert.That(intresult, Is.EqualTo(300));
        }


        [Test]
        public void MethodThatReturnsSomeRefType()
        {
            var obj = _factory.GetInatance<IFooo>("MyClass");
            obj.MyMethod();
            var result = obj.MethodThatReturnsSomething(55, new KeyValuePair<string, string>("Name", "Value"));
            Assert.That(result, Is.EqualTo("400"));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MethodThatReturnsSomeRefTypeThatThowsException()
        {
            var obj = _factory.GetInatance<IFooo>("MyClass");
            obj.MyMethod();
            var result = obj.MethodThatReturnsSomethingAndThowException(55, new KeyValuePair<string, string>("Name", "Value"));
            Assert.That(result, Is.EqualTo(null));
        }
        [Test]
        public void MethodThatReturnsSomeRefTypeThatThowsExceptionButExeptionEatenUp()
        {
            var obj = _factory.GetInatance<IFooo>("Foo");
            obj.MyMethod();
            var result = obj.MethodThatReturnsSomethingAndThowException(55, new KeyValuePair<string, string>("Name", "Value"));
            Assert.That(result, Is.EqualTo("Hello I ate the exeption"));
        }

        [Test]
        public void ShouldCallMethodThatReturnsSomeNotNullableValueWIthMoreThanOneInterceptor()
        {
            var obj = _factory.GetInatance<IFooo>("MyClass");
            var result = obj.MethodThatReturnsSomeNotNullableValueWIthMoreThanOneInterceptor(55, new KeyValuePair<string, string>("Name", "Value"));
            Assert.That(result, Is.EqualTo(100));
        }

        #region UnitTests

        [Test]
        public void ShouldCallInterceptorOnMethodCalled()
        {
            //var obj = _factory.GetInatance<IFooo>("MyClass");
            //var attributes = obj.GetType().GetMethod("MyMethod").GetCustomAttributes(typeof(AspectAttribute), true).Cast<AspectAttribute>();
            //attributes.ForEach(a => a.OnMethodExecuting(It.Is<MethodContext>()));
            //var mockRepository = new MockRepository(MockBehavior.Strict);
            //mockRepository.Create<AspectAttribute>();
            //obj.MyMethod();
        }
        #endregion
    }
}